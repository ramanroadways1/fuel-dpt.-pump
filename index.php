<?php
require_once 'connect.php';
require_once 'connect2.php';

$today=date('Y-m-d');
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>DIESEL PUMP USER : RRPL</title>
<meta http-equiv="refresh" content="60">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	<br />
	<br />
	
	<?php
	$fetch_record=mysqli_query($conn,"SELECT COUNT(id) as market_count,SUM(disamt) as market_sum FROM diesel_fm WHERE 
	pay_date='$today' AND dsl_by='PUMP' AND disamt>0");
	if(mysqli_num_rows($fetch_record)>0)
	{
		$row_market=mysqli_fetch_array($fetch_record);
		$market_count=$row_market['market_count'];
		$market_sum=$row_market['market_sum'];
	}
	else
	{
		$market_count=0;
		$market_sum=0;
	}
	
	$fetch_record_diary=mysqli_query($conn_diary,"SELECT count(id) AS own_count,SUM(diesel) as own_sum FROM diesel_entry WHERE 
	date='$today' AND card_pump='PUMP'");
	
	if(mysqli_num_rows($fetch_record_diary)>0)
	{
		$row_diary=mysqli_fetch_array($fetch_record_diary);
		$own_count=$row_diary['own_count'];
		$own_sum=$row_diary['own_sum'];
	}
	else
	{
		$own_count=0;
		$own_sum=0;
	}
	?>
	
	<div class="row">
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $market_sum; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $market_count; ?></b> MARKET Truck</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $own_sum; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;">
							<b><?php echo $own_count; ?></b> OWN Truck</div>
						</div>
					</div>
				</div>
			</div>	  
			
			 <div class="form-group col-md-3 col-sm-12">
		<form method="post" action="show_fm.php" autocomplete="off" target="_blank">
					<label style="color:#000">Search FM By Id <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="idmemo" required />
			   <input value="FM" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	
	<div class="form-group col-md-3 col-sm-12">
		<form method="post" action="show_fm.php" autocomplete="off" target="_blank">
					<label style="color:#000">Search FM By LRNo <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="lrno" required />
			   <input value="LR" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
			
</div>

<br />

<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#FFF;padding-top:5px;background:#06F;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i> RECENT <b>Market </b>Diesel</i></div>
	<div class="panel-body" style="overflow-x:hidden;">
<?php
$market = mysqli_query($conn,"SELECT branch,qty,rate,disamt,tno,dcard FROM diesel_fm WHERE pay_date='$today' AND dsl_by='PUMP'");

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>";
echo      "<tr>
				<th>Branch</th>
				<th>TruckNo</th>
				<th>Qty</th>
				<th>Rate</th>
				<th>Amt</th>
				<th>Code</th>
		</tr>";
if(mysqli_num_rows($market)>0)
{
while($row_m = mysqli_fetch_array($market))
  {
echo "<tr>
		<td>$row_m[branch]</td>			
		<td>$row_m[tno]</td>			
		<td>$row_m[qty]</td>			
		<td>$row_m[rate]</td>			
		<td>$row_m[disamt]</td>			
		<td>$row_m[dcard]</td>			
	</tr>";
}
}

echo  "</table>";
?>
					</div>
					
				</div>
				
			</div><!--/.col-->
	
		<div class="col-md-6 col-sm-12">
			
				<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#FFF;padding-top:5px;background:#06F;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i>RECENT <b>OWN </b> Diesel</i></div>
					<div class="panel-body" style="overflow-x:hidden;">
<?php
$own = mysqli_query($conn_diary,"SELECT de.tno,de.diesel,de.card,de.branch,d.qty,d.rate FROM diesel_entry as de 
LEFT OUTER JOIN diesel as d ON d.unq_id=de.unq_id AND d.narration=de.narration
WHERE de.date='$today' AND de.card_pump='PUMP'");

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>";
echo      "<tr>
				<th>Branch</th>
				<th>TruckNo</th>
				<th>Qty</th>
				<th>Rate</th>
				<th>Amt</th>
				<th>Code</th>
		</tr>";
if(mysqli_num_rows($own)>0)
{
while($rowt = mysqli_fetch_array($own))
  {
echo "<tr>
		<td>$rowt[branch]</td>			
		<td>$rowt[tno]</td>			
		<td>$rowt[qty]</td>			
		<td>$rowt[rate]</td>			
		<td>$rowt[diesel]</td>			
		<td>$rowt[card]</td>			
	 </tr>";
}
}
echo  "</table>";
?>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
</body>
</html>