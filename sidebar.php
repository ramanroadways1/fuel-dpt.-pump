<link href="https://fonts.googleapis.com/css?family=Baumans" rel="stylesheet">

<style>
::-webkit-scrollbar {
    width: 11px;
}
 
::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.2); 
    border-radius: 5px;
}
 
::-webkit-scrollbar-thumb {
    border-radius: 5px;
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.9); 
}
body{overflow-x:hidden;}
.nav>li>a{text-transform:lowercase; font-size:19px; font-weight:normal; font-family: 'Baumans', cursive;}
.navbar-header{font-family: 'Verdana', cursive;}
.navbar-inverse{background-color:#07F;
border-bottom:0px solid #ddd;
}
</style>
<style>

#title_msg  {
    display: none;
}

#title_msg2 {
    display: none;
}

@media screen and (max-width: 768px) {
    #title_msg {
        display: block;
    }


.navbar-default .navbar-nav>li>a{
color:#000;
}

.navbar-default .navbar-nav>li{
border-bottom:1px solid #ccc;
}

.navbar-default .navbar-nav>li>a:hover{
background-color:#000;
color:#fff;
}

}

@media screen and (min-width: 769px) {
    #title_msg2 {
        display: block;
    }

}


#cd{
	color:black;
	letter-spacing:1px;
	padding:7px;
	font-weight:bold;
	font-size:15px;
	margin-left:5px;
	font-family:Arial;
	 transition: color 0.3s linear;
   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
	
}
#cd:hover{
	background-color:#07F;
	color:#eee;
	
}
#newid>li>a:hover{
	background:#888;
	color:#fff;
	
}
#newid>li>a{
 transition: color 0.3s linear;
 color:#06F;

   -webkit-transition: background 0.3s linear;
   -moz-transition: background 0.3s linear;
}
</style>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./"><span></span>RRPL</a>
            <ul class="user-menu">
                <li class="dropdown pull-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <svg class="glyph stroked male-user">
                            <use xlink:href="#stroked-male-user"></use>
                        </svg> Diesel PUMP<span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                       <!-- <li>
                            <a href="#">
                                <svg class="glyph stroked male-user">
                                    <use xlink:href="#stroked-male-user"></use>
                                </svg> Profile</a>
                        </li>
                        <li>
                            <a href="#">
                                <svg class="glyph stroked gear">
                                    <use xlink:href="#stroked-gear"></use>
                                </svg> Settings</a>
                        </li> -->
                        <li>
                            <a href="logout.php">
                                <svg class="glyph stroked cancel">
                                    <use xlink:href="#stroked-cancel"></use>
                                </svg> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </div>
    <!-- /.container-fluid -->
</nav>
<div id="sidebar-collapse" class="col-sm-2 col-lg-2 sidebar">
    <br>
    <ul id="newid" class="nav menu" style="font-weight:bold; letter-spacing:0.5px;">
       <li class=""><a href="index.php">Dashboard</a></li>
	   <li><a href="./market/"><span style="text-transform:uppercase">M</span>arket Vehicle</a></li>
	   <li><a href="./e_diary/"><span style="text-transform:uppercase">O</span>wn Vehicle</a></li>
	   <li><a href="./logout.php"><span style="text-transform:uppercase">L</span>og out</a></li>
	  <!-- <li><a href="./mkt_trans.php"><span style="text-transform:uppercase">M</span>arket Trans. DB</a></li>
	   <li><a href="./own_trans.php"><span style="text-transform:uppercase">O</span>wn Trans. DB</a></li>-->
	</ul>

</div>