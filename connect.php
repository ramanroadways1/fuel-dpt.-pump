<?php
session_start();

include($_SERVER['DOCUMENT_ROOT']."/_connect.php");

if(!isset($_SESSION['diesel_pump']))
{
if(session_status()==1)
{
	session_start();
	session_destroy();
}	
  echo "<script>
	alert('Login Error...');
	window.location.href='/';
	</script>";
	exit();
}
	
date_default_timezone_set('Asia/Kolkata');

$conn=mysqli_connect($host,$username,$password,$db_name);

if(!$conn)
{
	if(session_status()==1)
	{
		session_start();
		session_destroy();
	}
	echo "DATABASE CONNECTION ERROR.";
	exit();
}
?>