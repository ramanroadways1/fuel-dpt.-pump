<?php 
require_once '../connect2.php';

$date2 = date("Y-m-d"); 
$max = date("Y-m-d");
$min = date("Y-m-d", strtotime("-30 day"));
?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
<center><img style="margin-top:150px" src="../load.gif" /></center>
</div>

<center><div id="result_1"></div></center>

</head>

<body style="background-color:#FFF">
<div style="margin-top:15px;" class="pull-left">
<span style="font-size:18px;color:#000;font-family:Verdana;margin-left:15px;margin-top:20px;">
<font color="red">PUMP SUMMARY : OWN Truck </font><?php //echo //strtoupper($_SESSION['manager']); ?></span>
</div>
<div class="pull-right" style="font-family:Verdana">
<a href="../"><button style="margin-right:5px;margin-top:15px;margin-right:5px" class="btn btn-danger">Go back</button></a>
</div>

<script type="text/javascript">
function MyFunc(elem)
{
	$("#new").show();
        $.ajax({
            type: "POST",
            url: "load_dsl.php", // 
            data: 'code=' + elem, // <---
            success: function(data){
                $("#load1").html(data)  
				$("#new").hide();
            },
            error: function(){
                alert("failure");
            }
        });
}
</script>

<div id="result_chk"></div>

<div class="container-fluid" style="font-family:Verdana">
<br />
<br />
<br />
<div class="col-md-6">
	<label>Select PUMP : </label>
	<select name="pump" onchange="MyFunc(this.value);" id="pump_name" class="form-control" required>
		<option value="">Select PUMP</option>
		<?php
		$fetch=mysqli_query($conn_diary,"SELECT name,code,comp FROM diesel_pump_own WHERE code!='' ORDER BY name ASC");
		if(mysqli_num_rows($fetch)>0)
		{
			while($row1=mysqli_fetch_array($fetch))
			{
				echo "<option value='$row1[code]'>$row1[name]-$row1[comp]</option>";
			}
		}
		?>
	</select>
</div>

<div class="col-md-12"><br><br></div>

<div class="col-md-12 table-responsive" id="load1">

</div>

</div>

</div>
</body>