<?php 
require_once '../connect2.php';

$code=$_POST['code'];

$qry = mysqli_query($conn_diary,"SELECT e.id,e.tno,e.branch,e.date,e.done,d.rate,d.qty,d.amount FROM diesel_entry AS e 
LEFT OUTER JOIN diesel AS d ON d.unq_id=e.unq_id AND d.narration=e.narration WHERE e.download!='1' AND e.diesel>0 AND e.card='$code' 
order by e.id ASC");
	
	if($qry)
	{
	if(mysqli_num_rows($qry)==0)
	{
	echo "<br />
	<h3 style='color:red'><center>
		No Pending Entry Found.</h3></center>";	
	}
	else
	{
		?>
<div id="result1"></div>

		<form action="download.php" target="_blank" method="POST">
			<input type="hidden" name="code" value="<?php echo $code ?>">
			<button type="submit" class="pull-right btn btn-warning">Export Excel</button>
		</form>
		<table class="table table-striped table-bordered" style="width:100%;font-size:13px;">	
	<thead>
		<tr>
			<th>Id</th> 
			<th>TruckNo</th> 
			<th>Qty</th> 
			<th>Rate</th> 
			<th>Amount</th> 
			<th>Branch</th> 
			<th>Date</th> 
			<th><button onclick="SelectAll('<?php echo $code; ?>')" class="btn btn-primary btn-sm">select all</button></th> 
		</tr>
	</thead>
	
	<script>
	function SelectAll(code)
	{
		$("#new").show();
        $.ajax({
            type: "POST",
            url: "select_all.php", // 
            data: 'code=' + code, // <---
            success: function(data){
                $("#result1").html(data)  
				$("#new").hide();
            },
            error: function(){
                alert("failure");
            }
        });
	}
	
	function Select(id)
	{
		$("#new").show();
        $.ajax({
            type: "POST",
            url: "select.php", // 
            data: 'id=' + id + '&code=' + '<?php echo $code; ?>', // <---
            success: function(data){
                $("#result1").html(data)  
				$("#new").hide();
            },
            error: function(){
                alert("failure");
            }
        });
	}
	
	function Reject(id)
	{
		$("#new").show();
        $.ajax({
            type: "POST",
            url: "reject.php", // 
            data: 'id=' + id + '&code=' + '<?php echo $code; ?>', // <---
            success: function(data){
                $("#result1").html(data)  
				$("#new").hide();
            },
            error: function(){
                alert("failure");
            }
        });
	}
	</script>
	
	<tbody>
	
	<?php
	$num=1;	
		while($row = mysqli_fetch_array($qry))
		{
			$datenew = date('d/m/y', strtotime($row['date']));
			
			$idnew = $row['id'];
			
			if($row['done']==1)
			{
				$btn= "
				<b><font color='green'>selected</font></b>
				<button onclick=Reject('$row[id]') class='btn btn-sm btn-danger'>unselect</button>";
			}
			else
			{
				$btn= "
				<button onclick=Select('$row[id]') class='btn btn-default btn-sm'>select</button>
				<b><font color='red'>unselected</font></b>";
			}
			
		echo "<input type='hidden' id='newid' value='$row[id]' />";
						
			echo "
			<tr>
			<td>$num</td>
			<td>$row[tno]</td>
			<td>$row[qty]</td>
			<td>$row[rate]</td>
			<td>$row[amount]</td>
			<td>$row[branch]</td>
			<td>$datenew</td>
			<td>
			$btn
			</td>
			</tr>
			";
		$num++;
		}
		echo "
		</tbody>
	</table>";
		
	}
	
	}
	else
	{
		echo mysqli_error($conn_diary);
	}
?>