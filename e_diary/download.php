<?php 
require_once '../connect2.php';
$date2 = date("Y-m-d"); 
$time_1=date('H:i:s');
$timestamp=$date2." ".$time_1;

$code=$_POST['code'];
$name=mysqli_query($conn_diary,"SELECT name FROM diesel_pump_own WHERE code='$code'");
$row_name=mysqli_fetch_array($name);
$name=$row_name['name'];

$output = '';

$query="SELECT e.tno,e.card,e.branch,e.date,e.done_time,d.rate,d.qty,d.amount FROM diesel_entry as e 
LEFT OUTER JOIN diesel AS d ON d.unq_id=e.unq_id AND d.narration=e.narration WHERE e.download!='1' AND e.done='1' AND e.card='$code' 
ORDER BY e.id ASC";
$result=mysqli_query($conn_diary,$query);
if(!$result)
{
	echo mysqli_error($conn_diary);
	exit();
}
if(mysqli_num_rows($result) > 0)
 {
 $output .= '
   <table border="1">  
					<tr>  
                         <td colspan="7" style="background:yellow">
						 <center><h3>OWN TRUCK DIESEL SUMMARY: <br> '.$name.'</h3><center></th>  
					 </tr>
					 
                    <tr>  
                         <th>TruckNo</th>  
                         <th>QTY</th>  
                         <th>Rate</th>
						 <th>Amount</th>  
                         <th>PumpCode</th>  
                         <th>Branch</th>  
                         <th>Date</th>  
                    </tr>
  ';
  while($row = mysqli_fetch_array($result))
  {
		
   $output .= '
						   <td>'.$row["tno"].'</td>  
						   <td>'.$row["qty"].'</td>  
						   <td>'.$row["rate"].'</td>  
						   <td>'.$row["amount"].'</td>  
						   <td>'.$row["card"].'</td>
						   <td>'.$row["branch"].'</td>
						   <td>'.$row["date"].'</td>
					</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_OWN_TRUCK.xls');
  echo $output;
  
  $q = mysqli_query($conn_diary,"UPDATE diesel_entry SET download='1',download_time='$timestamp' WHERE download!='1' AND done='1' 
  AND card='$code'");		
if(!$q)
{
	echo mysqli_error($conn_diary);
}
}
 else
 {
	 echo "<script>
		alert('No records found.');
		window.close();
		</script>";
 }
?>