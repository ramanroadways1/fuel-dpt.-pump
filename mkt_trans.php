<html>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<a href="../"><button style="margin:10px" class="btn btn-danger">Go back</button></a>

<style>
.form-control{
	text-transform:uppercase;
}
</style>

<div class="container" style="font-family:Verdana">

<form autocomplete="off" action="export.php" method="POST">

	<div class="form-group col-md-4 col-md-offset-1">

	<div class="form-group col-md-12">
		<center><h4>Market Truck Wise Trans.</h4></center>
	</div>

	<div class="form-group col-md-12">
		<label>Truck No <font color="red"><sup>*</sup></font></label>
		<input name="tno" type="text" class="form-control" required />
	</div>
	
	<div class="form-group col-md-12">
		<label>From date <font color="red"><sup>*</sup></font></label>
		<input name="from_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
	</div>
	
	<div class="form-group col-md-12">
		<label>To date <font color="red"><sup>*</sup></font></label>
		<input name="to_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
	</div>
	
	<div class="form-group col-md-12">
		<input type="submit" value="Export excel" class="btn btn-block btn-danger" />
	</div>
</form>	

</div>

<form autocomplete="off" action="export2.php" method="POST">

	<div class="form-group col-md-4 col-md-offset-2">

	<div class="form-group col-md-12">
		<center><h4>Market Date wise Trans.</h4></center>
	</div>

	<div class="form-group col-md-12">
		<label>From date <font color="red"><sup>*</sup></font></label>
		<input name="from_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
	</div>
	
	<div class="form-group col-md-12">
		<label>To date <font color="red"><sup>*</sup></font></label>
		<input name="to_date" type="date" class="form-control" pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" max="<?php echo date("Y-m-d"); ?>" required />
	</div>
	
	<div class="form-group col-md-12">
		<input type="submit" value="Export excel" class="btn btn-block btn-danger" />
	</div>
</form>	

</div>
</html>